//******************************************************//
//******************************************************//
//****												****//
//****					T�lt�csiga1					****//
//****												****//
//******************************************************//
//******************************************************//



#include "AT91SAM7X256.H"
#include "csiga.h"
#include "Tolto.h"
#include "Board.h"
#include "data.h"
#include "expander.h"


volatile csiga_ cs1;


void csiga1Timer(void)
{
 	cs1.felfutTimer++;
	cs1.motorvedoTimer++;
}

void csiga1_start(void){cs1.mehet = 1;}										//Ind�t�s

void csiga1_stop(void){cs1.mehet = 0;cs1.error_state = CSIGA_NOT_OK;}		//Meg�ll�t�s



int csiga1(void)
{
	cs1.forgTimer++;

	if(getClearGomb())														//Hibat�rl�s
	{
		cs1.proba_counter = 0;
		cs1.probalkozik = 0;
	 	cs1.elakadt = 0;	
	}

	if(getKezi()&&(TOLTO1_MOTORVEDO))	 									//K�zi
	{
	 	if(getTolto1EloreGomb()){TOLTO1_ELORE_ON;}
		else{TOLTO1_ELORE_OFF;}

		if(getTolto1HatraGomb()){TOLTO1_HATRA_ON;}
		else{TOLTO1_HATRA_OFF;}

		cs1.proba_counter = 0;
		cs1.probalkozik = 0;
	 	cs1.elakadt = 0;
		cs1.start = 0;

		cs1.error_state = CSIGA_OK;
	}

//Automata

	if(getAutomata() || getFelautomata())	 								//Automata	vagy f�lautomata
	{
		if((cs1.mehet == 1)&&(cs1.probalkozik == 0)&&(cs1.elakadt == 0)&&(TOLTO1_MOTORVEDO))
		{
			if(cs1.start == 0)			   									//Norm�l el�remenet indul�s
			{
			 	cs1.start = 1;
				cs1.felfutTimer = 0;
				cs1.felfutas = 1;

				cs1.error_state = CSIGA_NOT_OK;
			}

			if(cs1.felfutTimer > START_SZUNET)								//Sz�net indul�s el�tt
			{
			 	TOLTO1_ELORE_ON;
			}

			if(cs1.felfutTimer > getTolto1Felfutas()+START_SZUNET)
			{
				cs1.felfutas = 0;											//Felfut�s id� letelt
			}

			if(cs1.felfutTimer > (getTolto1Felfutas()+START_SZUNET+ELAKADASMENTES))
			{
			 	cs1.proba_counter = 0;									   	//Elakad�smentes el�remenet 
			}
			
			if(TOLTO1_FORGASFIGYELO)										//Forg�sfigyel� bekapcsolva
			{
			   if(cs1.forg_state == 0)										//Felfut� �l
			   {
					cs1.forg_state = 1;
					cs1.forgTimer = 0;
			   }
			}

			if(!TOLTO1_FORGASFIGYELO)										//Forg�sfigyel� kikapcsolva
			{
			 	cs1.forg_state = 0;	
			}

			if((cs1.felfutas == 0)&&(cs1.forgTimer > getTolto1Forgido()))
			{
				TOLTO1_ELORE_OFF;
				cs1.start = 0;
			 	cs1.probalkozik = 1;
				cs1.proba_counter++;
			}


		}	 //norm�l el�remenet
		
		
		if((cs1.mehet == 1)&&(cs1.probalkozik == 1)&&(cs1.elakadt == 0)&&(TOLTO1_MOTORVEDO))	//Ha pr�b�lkozik
		{
		 	 if(cs1.start == 0)
			 {
			  	cs1.start = 1;
				cs1.felfutTimer = 0;
			 }

			 if(cs1.felfutTimer > START_SZUNET)
			 {
			  	TOLTO1_HATRA_ON;
			 }

			 if(cs1.felfutTimer > START_SZUNET+HATRAMENET)
			 {
			  	TOLTO1_HATRA_OFF;
				cs1.probalkozik= 0;
				cs1.start = 0;
			 }
		}//Pr�b�lkoz�s, h�tramenet	


	}


	if(cs1.probalkozik == 1){cs1.error_state = CSIGA_PROBALKOZIK;}
	if(cs1.proba_counter > getProbalkozasSzam()){cs1.elakadt = 1;cs1.error_state = CSIGA_ELAKADT;}
	if((cs1.felfutTimer > START_SZUNET + getTolto1Felfutas()) && (cs1.proba_counter == 0 )){cs1.error_state = CSIGA_OK;}

	if(!TOLTO1_MOTORVEDO)	  								//Motorv�d� lekapcsolt
	{
		cs1.start = 0;
		cs1.error_state = CSIGA_NOT_OK;
		cs1.probalkozik = 0;
		TOLTO1_ELORE_OFF;
		TOLTO1_HATRA_OFF;

		if(cs1.motorvedo_start == 0)
		{
		 	cs1.motorvedo_start = 1;
			cs1.motorvedoTimer = 0;
		}

		if(cs1.motorvedoTimer > MOTORVEDO_IDO)				//Ha hossz� id� ut�n sem kapcsol vissza
		{
		 	  set_expander_outputs(16);
		}
	}

	if(TOLTO1_MOTORVEDO)		 							//Motorv�d� visszakapcsolt
	{
	 	cs1.motorvedo_start = 0;
		clear_expander_outputs(16);
	}

	return cs1.error_state;	
}