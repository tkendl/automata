#include "AT91SAM7X256.H"
#include "kazan.h"
#include "Board.h"
#include "data.h"
#include "expander.h"


volatile int KazanMehet;
volatile int rostely_be, rostely_szunet;
volatile int rostely_timer,bolygato_timer,timer_on_timer,timer_szunet_timer;
volatile int szivattyu_timer,szivattyu_timer_start;
volatile int rostely_timer_start,rostelymegy;
volatile int bolygato_timer_start,bolygatomegy;
volatile int bolyg_ido;
volatile int UzemiBolygatas,AlloBolygatas,VeszBolygatas;
volatile int kialvasgatlo_start, kialvasgatlo_be;
volatile int visszahutes;
volatile int kihules_timer, kihules_timer_start;
volatile int tuzoltas_timer, tuzoltas_start;

volatile int kulso_szivattyu_tilt;



void kazanTenthTimer(void)
{
    rostely_timer++;
}

void kazanSecTimer(void)
{
    bolygato_timer++; 
    timer_on_timer++; 
    szivattyu_timer++; 
    kihules_timer++;
	tuzoltas_timer++;
}

void kazanMinTimer(void)
{
    timer_szunet_timer++;
}

void kazan(void)
{  
//Rost�lysebess�g kisz�m�t�sa
	double x;
	if (getRostelyseb() == 0)
            {
                rostely_szunet = 0;
                rostely_be = 0;
            }
            else
            {
                rostely_be = 50;
                x = (double)getRostelyseb();
                x = 100 / x;
                x = x - 1;
                x *= 50;
                rostely_szunet = (int)x;
            }
//----------------------------------------------------------------------------

	if(getKezi() > 0)                                               //Ha k�zi
	{
	 	if(getRostelyGomb() > 0){ROSTELY_ON;}
		else{ROSTELY_OFF;}

		if(getVentiGomb() > 0){VENTI_ON;}
		else{VENTI_OFF;}

		if(getBolygGomb() > 0){BOLYG_ON;bolygatomegy = 1;}
		else{BOLYG_OFF;bolygatomegy = 0;}

		if(getBelimoGomb() > 0){BELIMO_ON;}
		else{BELIMO_OFF;}

		if(getSzivattyuGomb() > 0){SZIVATTYU_ON;}
		else{SZIVATTYU_OFF;}
	}
    
    
    if(getKezi() == 0)                                                //Automata �zem
    {
        if(KazanMehet == 1)
        {
            szivattyu_timer = 0;
            szivattyu_timer_start = 1;
            if(rostelymegy == 0)
            {
                if(rostely_timer_start == 0)                          //Rost�ly ind�t�sa
                {
                    rostely_timer = 0;
                    rostely_timer_start = 1;
                }   
            

                 if(rostely_timer > rostely_szunet)
                {
                     if((rostely_be > 0) && (getFauzem() == 0))
                    {
                         ROSTELY_ON;
                    }
                    rostelymegy = 1;
				    rostely_timer_start = 0;
                }
            }

        }

        if(rostelymegy == 1)
        {
            if(rostely_timer_start == 0)						        //Rost�ly meg�ll�t�sa
			{
				rostely_timer = 0;
				rostely_timer_start = 1;
			}

			if(rostely_timer > rostely_be)
			{
				ROSTELY_OFF;
				rostelymegy = 0;
				rostely_timer_start = 0;
			}	
        }


        if(KazanMehet == 1 && !FELEGES && (getFauzem() == 0))	        //�zemi bolygat�s
	    {
		    bolyg_ido = getUzemiBolyg() * 10;
	    }

	    if(((KazanMehet == 0) || (getFauzem() > 0)) && !FELEGES)		//�ll�helyzeti bolygat�s
	    {
	    	bolyg_ido = getAlloBolyg() * 60;
	    }

        if(bolygatomegy == 0)
		{
			if(bolygato_timer_start == 0)								//Bolygat� ind�t�sa
			{
				bolygato_timer = 0;
				bolygato_timer_start = 1;
			}

			if(bolygato_timer >= bolyg_ido)
			{
				if(rostely_be > 0)				                    	//0 sebess�gn�l bolygat� meg�ll�t�sa
				{
					BOLYG_ON;
				}
				bolygatomegy = 1;
				bolygato_timer_start = 0;
			}		
		}
        
        if(bolygatomegy == 1)
	    {
		    if(bolygato_timer_start == 0)								//Bolygat� meg�ll�t�sa
			{
				bolygato_timer = 0;
				bolygato_timer_start = 1;
			}

			if(bolygato_timer >= getBolygBe())
			{
				BOLYG_OFF;
				bolygatomegy = 0;
				bolygato_timer_start = 0;
			}	
	    }

		if(KazanMehet == 1)
        { 
            VENTI_ON;
            BELIMO_ON;
        }

        if(KazanMehet == 0)
        { 
            VENTI_OFF;
			if(AJTO_NYITVA)
			{
				BELIMO_ON;
			}
			else{BELIMO_OFF;}
        }

        if(BOVITO_ARAMSZUNET && !VESZTERMOSZTAT && (getHomero1() < 117) )//&& (SZOBATERMOSZTAT || kialvasgatlo_be))
        {
            if( getMelegentart() > 0)                                       //Melegentart�s bekapcsolva
            {
                if(getHomero1() >= getHofok1())
                {
                    KazanMehet = 0;                                         //Kiapcs ha felf�t�tt
                }
                if(getHomero1() < (getHofok1()-getVizhoHyst()))             //Bekapcs ha leh�lt
                {
                    KazanMehet = 1;
                }
            }

            if(( getMelegentart() == 0) &&  SZOBATERMOSZTAT )               //Melegentart�s kikapcsolva
            {
                if(getHomero1() >= getHofok1())
                {
                    KazanMehet = 0;                                         //Kiapcs ha felf�t�tt
                }
                if(getHomero1() < (getHofok1()-getVizhoHyst()))             //Bekapcs ha leh�lt
                {
                    KazanMehet = 1;
                }
            }

            if(( getMelegentart() == 0) &&  !SZOBATERMOSZTAT )
            {
                KazanMehet = 0;
            }

            if(kialvasgatlo_be)                                             //Kialv�sg�tl�s
            {
                if(getHomero1() >= getHofok1())
                {
                    KazanMehet = 0;                                         //Kikapcs ha felf�t�tt
                }
                if(getHomero1() < (getHofok1()-getVizhoHyst()))             //Bekapcs ha leh�lt
                {
                    KazanMehet = 1;
                }
            }
            
             
        }
        else{ KazanMehet = 0;}


        if(FELEGES || FELEGES_FELSO)                                       	//V�szbolygat�s
        {
            if(getBurninEnable() == 0)                                      //Fel�g�skor kaz�n mehet vagy nem
            {
                KazanMehet = 0;
            }
        
            bolyg_ido = 30;
        }

		if(FELEGES_FELSO)		  											//T�zolt�s
		{
			if(!tuzoltas_start)
			{
				tuzoltas_start = 1;
				tuzoltas_timer = 0;
				TUZOLTAS_ON;
			}
			
		}
		else{TUZOLTAS_OFF;tuzoltas_start = 0;}

		if(tuzoltas_start && (tuzoltas_timer > (getTuzoltasTime() * 60)))
		{
			TUZOLTAS_OFF;
		}

        if(TARTALY_URES)
        {
            if(getLowlevelEnable() == 0)                                    //Ha �res a tart�ly, mehet-e?
            {
                KazanMehet = 0;
            }
        }

		if(getSalakol() || !AJTO_CSUKVA || FELEGES_FELSO)// || !VENTI_MV
		{
		 	KazanMehet = 0;	
			kulso_szivattyu_tilt = 1;
		}
		else{kulso_szivattyu_tilt = 0;}



		if(!VENTI_MV && BOVITO_LEKAPCS){setVentilatorMVError();}	 					//Ventil�tor MV
		else{clearVentilatorMVError();}

        if(SZOBATERMOSZTAT || visszahutes || ((KazanMehet == 1) && (getRoomTherPresent() == 0)))
        {
            SZIVATTYU_ON;
        }
        else{SZIVATTYU_OFF;}
    
        if(getHomero1() >= (getHofok1() + getTulfutHyst())){ visszahutes = 1;}         //Ha t�lmelegszik szivatty� bekapcs
        if(visszahutes && ( getHomero1() <= getHofok1())) { visszahutes = 0;}          //Ha visszah�l szivatty� kikapcs

		if(kulso_szivattyu_tilt && !visszahutes)
		{
		 	SZIVATTYU_TILTAS_ON;
		}
		else{SZIVATTYU_TILTAS_OFF;}


    }//automata	
    
    if(KazanMehet == 1){timer_szunet_timer = 0;}
    if(timer_szunet_timer > (getTimerSzunet()*60))
    {
        if(kialvasgatlo_start == 0)                                              		//Kialv�sg�tl�
        {
            kialvasgatlo_start = 1;
            timer_on_timer = 0;
            kialvasgatlo_be = 1;
        }
    }
    
    if(kialvasgatlo_be == 1)
    {
        if(timer_on_timer > (getTimerBe()*60))
        {
             kialvasgatlo_start = 0;
             kialvasgatlo_be = 0;
        }
    }

    


 //F�stg�z kih�l�s                                                                  //Ha megy a kaz�n �s nem fauzem
    if((KazanMehet == 1) && (rostely_be > 0))                                                                   
    {
        if(kihules_timer_start == 0)
        {
            kihules_timer_start = 1;                                                //Id�z�t� ind�t�sa
            kihules_timer = 0;
        }

        if(kihules_timer >= (getKihulIdo() * 60))                                   //Ha lej�rt az id�
        {
              if(getHomero2() < getKihulHyst())                                     //�s a f�stg�z h�fok alacsony
              {
              	if(getFusthomeroPresent())
				{		 	 	  
				  	setKihultError(); 
				}		 
              }
        }
    }
    else
    {
        kihules_timer_start = 0; 
    }

    if(getClearGomb())
    {
        clearKihultError();
        kihules_timer_start = 0;
    }

    if(getKihultError())
    {
        KazanMehet = 0;
    } 
	
	if(getHomero1() > 95){setOverheatError();}
	else{clearOverheatError();} 
	
	if(FELEGES_FELSO){setTuzoltasError();}
	else{clearTuzoltasError();}  
   
}
