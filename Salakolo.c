#include "AT91SAM7X256.H"
#include "Tolto.h"
#include "csiga.h"
#include "kazan.h"
#include "Board.h"
#include "Ajto.h"
#include "data.h"
#include "expander.h"
#include "Pins.h"

volatile int pernye_state,salak_state,pockolo_state, felhordo1_state;
volatile csiga_ pernyeCS,salakCS,pockoloCS,felhordoCS1;
volatile csiga_ * pernyeCSp,* salakCSp,* pockoloCSp,* felhordoCS1p;

volatile int Salakolas,salakszunet_timer,salakbe_timer,salakgomb_state,salakActido_tmp,salakszunetActIdo_tmp;
volatile int pockolo_leallt_vegallasban, pernye_leallt_vegallasban;
volatile pockolo_leall_timer, pernye_leall_timer;
volatile salakolas_fatal_error;

volatile int ize;

volatile int felhordo_utanfutas, felhordo_utanfutas_start, felhordo_utanfutas_timer,pockoles;

void salakInit(void)
{
//Pernyecsiga
	pernyeCS.forgido_mul = 10;	   					//Forgid� szorz�
	pernyeCS.Elore = _pernyeElore;	
	pernyeCS.Hatra = _pernyeHatra;
	pernyeCS.Allj = _pernyeAllj;
	pernyeCS.Motorvedo = _pernyeMV;
	pernyeCS.Forgasfigyelo = _pernyeFF;	
	pernyeCS.aramfigyeles = 1;							
	//pernyeCS.get_felfutas = getPernyeFelfutas;
	pernyeCS.get_forgido = getPernyezoForgido;
	pernyeCS.get_aramkorlat = getPernyecsigaAramkorlat;
	pernyeCS.get_arammero = getPernyeajtoArammero;
	pernyeCS.get_eloregomb = getPernyeEloreGomb;
	pernyeCS.get_hatragomb = getPernyeHatraGomb;

	pernyeCSp = &pernyeCS;

//Salakcsiga
	salakCS.forgido_mul = 1;	   					//Forgid� szorz�
	salakCS.Elore = _salakElore;	
	salakCS.Hatra = _salakHatra;
	salakCS.Allj = _salakAllj;
	salakCS.Motorvedo = _salakMV;
	salakCS.Forgasfigyelo = _salakFF;								
//	salakCS.get_felfutas = getSalakoloFelfutas;
	salakCS.get_forgido = getSalakoloForgido;
	salakCS.get_eloregomb = getSalakoloEloreGomb;
	salakCS.get_hatragomb = getSalakoloHatraGomb;

	salakCSp = &salakCS;

//P�ck�l�
	pockoloCS.forgido_mul = 10;	   					//Forgid� szorz�
	pockoloCS.Elore = _pockoloElore;	
	pockoloCS.Hatra = _pockoloHatra;
	pockoloCS.Allj = _pockoloAllj;
	pockoloCS.Motorvedo = _pockoloMV;
	pockoloCS.Forgasfigyelo = _pockoloFF;
	pockoloCS.aramfigyeles = 1;								
//	pockoloCS.get_felfutas = getPockoloFelfutas;
	pockoloCS.get_forgido = getPockoloForgido;
	pockoloCS.get_aramkorlat = getPockoloAramkorlat;
	pockoloCS.get_arammero = getSalakajtoArammero;
	pockoloCS.get_eloregomb = getPockoloEloreGomb;
	pockoloCS.get_hatragomb = getPockoloHatraGomb;

	pockoloCSp = &pockoloCS;

//Felhord�csiga1
	felhordoCS1.forgido_mul = 1;	   					//Forgid� szorz�
	felhordoCS1.Elore = _felhordo1Elore;	
	felhordoCS1.Hatra = _felhordo1Hatra;
	felhordoCS1.Allj = _felhordo1Allj;
	felhordoCS1.Motorvedo = _felhordo1MV;
	felhordoCS1.Forgasfigyelo = _felhordo1FF;								
//	felhordoCS1.get_felfutas = getFelhordo1Felfutas;
	felhordoCS1.get_forgido = getFelhordo1Forgido;
	felhordoCS1.get_eloregomb = getFelhordo1EloreGomb;
	felhordoCS1.get_hatragomb = getFelhordo1HatraGomb;

	felhordoCS1p = &felhordoCS1;

}
//tizedm�sodperc
void salakTenthTimer(void)
{
	csigaTimer(pernyeCSp);
	csigaTimer(salakCSp);
	csigaTimer(pockoloCSp);
	csigaTimer(felhordoCS1p);	
}
//m�sodperc
void salakSecTimer(void)
{
	salakbe_timer++;
	felhordo_utanfutas_timer++;
	//salakszunet_timer++;
}

//perc
void salakMinTimer(void)
{
	salakszunet_timer++; 	
}

void setSalakszunetTimer(int i)
{
 	salakszunet_timer = i;	
}

int getSalakszunetTimer(void)
{
 	return salakszunet_timer;
}

int getSalakol(void) 								//�ppen salakol-e?	  (kaz�nt meg kell �ll�tani)
{
 	if(Salakolas){return 1;}
	else{return 0;}
}


void Salakolo(void)
{	
	if(getFelhordoInstalled())
	{
		felhordo1_state = csiga(felhordoCS1p);
	}
	salak_state = csiga(salakCSp);
	pockolo_state = csiga(pockoloCSp);
	pernye_state = csiga(pernyeCSp);
	
	if(getClearGomb())														//Hibat�rl�s
	{
	 	salakolas_fatal_error = 0;	
	}
	
			
//K�zi
	if(getKezi())
	{
	 	Salakolas = 0;
		pockolo_leallt_vegallasban = 0;
		pernye_leallt_vegallasban = 0;		
	}
	if(getSalakoloEloreGomb()){set_expander_outputs(15);}
	else{clear_expander_outputs(15);}

//Automata
	if(!getKezi())
	{
	 	if(!Salakolas)
		{
		 	if(getSalakoloEloreGomb() && !salakgomb_state)	 //Salakol�s manu�lis ind�t�sa
			{	
				if(felhordo_utanfutas)
				{
					salakgomb_state = 1;
				 	felhordo_utanfutas = 0;
					felhordo_utanfutas_start = 0;  
					pockoles = 0;		
				}
				else
				{
					salakgomb_state = 1;  
			 		salakbe_timer = 0;
					Salakolas = 1;
				}
			}
		}

		
		if(Salakolas)
		{
		 	if(getSalakoloEloreGomb() && !salakgomb_state)	   //Salakol�s meg�ll�t�sa
			{
			 	salakgomb_state = 1;
				Salakolas = 0;
				felhordo_utanfutas = 0;
				felhordo_utanfutas_start = 0;
				pockoles = 0;	
			}
		}	
		
		if(!Salakolas)								  						//Ha letelt a sz�netid� salakol�s ind�t�sa
		{
		 	//if((salakszunet_timer >= getSalakSzunet()) && !salakolas_fatal_error);
			if(salakszunet_timer >= getSalakSzunet())
			{
				if(!salakolas_fatal_error)
				{
			 		Salakolas = 1;
					salakbe_timer = 0;
				}
			}
		}	
	
	} //Automata

	if(!getSalakoloEloreGomb()){salakgomb_state = 0;}

//Salakol�s
	if(Salakolas)
		{	salakszunet_timer = 0;
	 		felhordoCS1.mehet = 1;											//Felhord� indulhat
			setCANSalakoloMehet();

			if((felhordo1_state == CSIGA_OK)||(getCANSalakoloOK()))									
			{
				if(!felhordo_utanfutas)									   	//Ha felhord� ok �s nem ut�nfut�s, salak indulhat
				{
					salakCS.mehet = 1;
				}
				if(pockoles)											   				//Ha felhord� ok �s p�ck�l�s, p�ck�l� indulhat
				{
					pockoloCS.mehet = 1;
					pockolo_leallt_vegallasban = 0;
				}
			}			
			else{pockoloCS.mehet = 0;salakCS.mehet = 0;}			


			if((salak_state == CSIGA_OK) && (!pockoles))					//ha salakol�s, de nem p�ck�l�s, pernyecsiga indulhat						
			{
				pernyeCS.mehet = 1;
				pernye_leallt_vegallasban = 0;
			}				
			else{pernyeCS.mehet = 0;}
		
			if(salakbe_timer > (getSalakBe()*40))//*60/3*2					//2/3-ad id�n�l pernye �llj, p�ck�l� mehet
			{
			 	pockoles = 1;
			}
			
			if(pockoles)
			{
			 	if((felhordo1_state == CSIGA_OK)||(getCANSalakoloOK()))		//Ha felhord� ok							
				{															//P�ck�l� is mehet
			 		pockoloCS.mehet = 1;
					pockolo_leallt_vegallasban = 0;
				}
				else{pockoloCS.mehet = 0;}	
			}										  
		 	if(salakbe_timer > (getSalakBe()*60))							//Ha letelt a salakol�si id� akkor le�ll�t�s
			{
			 	Salakolas = 0;
				salakszunet_timer = 0;
					
				felhordo_utanfutas_timer = 0;								//Felhordo ut�nfut�s start
				felhordo_utanfutas = 1;
			}


		}//Salakolas  


		if(felhordo_utanfutas) 														//Felhord� ut�nfut�s
		{
			felhordoCS1.mehet = 1;													//Felhord� indulhat
			setCANSalakoloMehet();

			if(felhordo1_state == CSIGA_OK)									//Ha felhord� ok							
			{																	  						//P�ck�l� is mehet
			 	pockoloCS.mehet = 1;
				pockolo_leallt_vegallasban = 0;
			}
			else{pockoloCS.mehet = 0;}	

		 	if(felhordo_utanfutas_timer > (getFelhordoUtanfutas()*60))	  									
			{
			 	felhordo_utanfutas = 0;
				felhordo_utanfutas_start = 0;
				felhordoCS1.mehet = 0;
				clearCANSalakoloMehet();
				pockoloCS.mehet = 0;
				pockoles = 0;
			}		
		}
		
//Salakol�s v�ge, le�ll�t�s
		if((!Salakolas && !getKezi()) || !VEDOBERENDEZESEK_KORE || !BOVITO_LEKAPCS)	//Ha nyitva a salakajt� 
		{																	  						//vagy nyitva az ajt�
		 	pernyeCS.mehet = 0;														//vagy  �ramsz�net/lekapcsolt�k
			salakCS.mehet = 0;								 						//akkor salak �llj

			if(!felhordo_utanfutas || !VEDOBERENDEZESEK_KORE || !BOVITO_LEKAPCS)
			{
				felhordoCS1.mehet = 0;
				clearCANSalakoloMehet();
				pockoloCS.mehet = 0;
			}



			if(!pockolo_leallt_vegallasban && (!felhordo_utanfutas  && !Salakolas))//&& (felhordo1_state == CSIGA_OK)))
			{
		 		if(!_pockoloFF()){pockoloCS.mehet = 1;}								//V�g�ll�sig megy
				else
				{
					pockoloCS.mehet = 0;
					pockolo_leallt_vegallasban = 1;
				}
			}	
			
			if(!pernye_leallt_vegallasban)
			{
		 		if(!_pernyeFF()){pernyeCS.mehet = 1;}	  							//V�g�ll�sig megy
				else
				{
					pernyeCS.mehet = 0;
					pernye_leallt_vegallasban = 1;
				}
			}		
		} 

		//Ha stopgombbal (k�perny�n) meg�ll�tva, salakol�s stop
		if(getSalakolorendszerTiltas())
		{
		 	Salakolas = 0;
			felhordoCS1.mehet = 0;
			clearCANSalakoloMehet();
			pockoloCS.mehet = 0;
			salakCS.mehet = 0;
			pernyeCS.mehet = 0;
			//salakbe_timer = 0;
			//salakszunet_timer = 0;
		}

		//Salakol�s sz�netid� �s salakol�sb�l h�tral�v� id� elk�ld�se
		salakszunetActIdo_tmp = getSalakSzunet() - salakszunet_timer;
		if(Salakolas)
		{
		 	salakActido_tmp = (((getSalakBe()*60) - salakbe_timer) + (getFelhordoUtanfutas()*60)); 
			salakszunetActIdo_tmp = 65000;	
		}

		if(felhordo_utanfutas)
		{
		 	salakActido_tmp = ((getFelhordoUtanfutas()*60) - felhordo_utanfutas_timer);
			salakszunetActIdo_tmp = 65000;	
		}

		if(!Salakolas && !felhordo_utanfutas)
		{
		 	salakActido_tmp = 65000;	
		}

		if(salakolas_fatal_error){ salakActido_tmp = 65000; salakszunetActIdo_tmp = 65000;}
		setSalakActIdo(salakActido_tmp);

		
		setSalakszunetActIdo( salakszunetActIdo_tmp );
	
	   if(Salakolas){setSalakolasmegy();}else{clearSalakolasmegy();}

//Hib�k elk�ld�se
	if(getFelhordoInstalled())
	{
	 	if(felhordo1_state == CSIGA_PROBALKOZIK){setFelh1ProbalError();}else{clearFelh1ProbalError();}
		if(felhordo1_state == CSIGA_ELAKADT){setFelh1ElakadtError();salakolas_fatal_error = 1;}else{clearFelh1ElakadtError();}
		//if(!FELHORDO1_MOTORVEDO && BOVITO_LEKAPCS){setFelh1MVError();}else{clearFelh1MVError();}
		if(felhordo1_state == MV_NEMKAPCSOLT_VISSZA){setFelh1MVnemkapcsoltError();}else{clearFelh1MVnemkapcsoltError();}
	}
	else
	{
		clearFelh1ProbalError();
		clearFelh1ElakadtError();
		//clearFelh1MVError();
		clearFelh1MVnemkapcsoltError();
	}
	
	if(salak_state == CSIGA_PROBALKOZIK){setSalakProbalError();}else{clearSalakProbalError();}
	if(salak_state == CSIGA_ELAKADT){setSalakElakadtError();salakolas_fatal_error = 1;}else{clearSalakElakadtError();}

	if(pockolo_state == CSIGA_PROBALKOZIK){setPockoloProbalError();}else{clearPockoloProbalError();}
	if(pernye_state == CSIGA_PROBALKOZIK){setPernyeProbalError();}else{clearPernyeProbalError();}

	if(pockolo_state == CSIGA_ELAKADT){setPockoloElakadtError();pockoloCS.mehet = 0;}else{clearPockoloElakadtError();}
	if(pernye_state == CSIGA_ELAKADT){setPernyeElakadtError();pernyeCS.mehet = 0;}else{clearPernyeElakadtError();}

	
	//if(!SALAKOLO_MOTORVEDO && BOVITO_LEKAPCS){setSalakMVError();}else{clearSalakMVError();}	
	if(salak_state == MV_NEMKAPCSOLT_VISSZA){setSalakMVnemkapcsoltError();}else{clearSalakMVnemkapcsoltError();}

	if(!VEDOBERENDEZESEK_KORE){setErrorSalakajtoNyitva();} else{clearErrorSalakajtoNyitva();}
	

	if((felhordo1_state == CSIGA_ELAKADT) || (felhordo1_state == MV_LEKAPCSOLT) || (felhordo1_state == MV_NEMKAPCSOLT_VISSZA) ||
	  (salak_state == CSIGA_ELAKADT) || (salak_state == MV_LEKAPCSOLT) || (salak_state == MV_NEMKAPCSOLT_VISSZA) ||
	  (pockolo_state == CSIGA_ELAKADT) || (pernye_state == CSIGA_ELAKADT))
	  {setCANSalakoloHiba();}else{clearCANSalakoloHiba();}

}
