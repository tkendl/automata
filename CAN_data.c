#include "AT91SAM7X256.H"
#include "CAN_data.h"
#include "Board.h"
#include "expander.h"
#include "data.h"

unsigned char CANRxdata50[20];
unsigned char CANRxdata80[20];
unsigned char CANTxdata[20];


void setCANrx_data(int ID, unsigned char buff[])
{
	int i;
	switch (ID)
	{
		case 50:
			for(i=0;i<8;i++){CANRxdata50[i] = buff[i];} break;
			
		case 80:
			for(i=0;i<8;i++){CANRxdata80[i] = buff[i];} break;
	
}
} 

void setCANtx_data(int page, unsigned char buff[])
{
	int i, start;

	if(page == 0x00){start = 0;}
	if(page == 0X01){start = 7;}

	buff[0] = page;
	for(i=0;i<7;i++){buff[i+1] = CANTxdata[i+start];}
}


void refreshCANdata(void)
{
	int i;

	i=getHomero1();
	CANTxdata[0] = i>>8;
	CANTxdata[1] = i;

	i=getHomero2();
	CANTxdata[2] = i>>8;
	CANTxdata[3] = i;

	i=getHofok1();
	CANTxdata[7] = i>>8;
	CANTxdata[8] = i;

	i=getImitActIdo();
	CANTxdata[9] = 	i>>8;
	CANTxdata[10] = i;

	CANTxdata[11] = getRostelyseb();



	if(getKezi())						{CANTxdata[5] |= (1<<0);} else{CANTxdata[5] &= ~(1 << 0);}
	if(TARTALY_URES)					{CANTxdata[5] |= (1<<1);} else{CANTxdata[5] &= ~(1 << 1);}
	if(TARTALY_TELE)					{CANTxdata[5] |= (1<<2);} else{CANTxdata[5] &= ~(1 << 2);}
	if(AJTO_NYITVA)						{CANTxdata[5] |= (1<<3);} else{CANTxdata[5] &= ~(1 << 3);}
	if(AJTO_CSUKVA)						{CANTxdata[5] |= (1<<4);} else{CANTxdata[5] &= ~(1 << 4);}
	if(VEDOBERENDEZESEK_KORE)			{CANTxdata[5] |= (1<<5);} else{CANTxdata[5] &= ~(1 << 5);}

	if(getToltorendszerTiltas())		{CANTxdata[4] |= (1<<3);} else{CANTxdata[4] &= ~(1 << 3);}
	if(getSalakolorendszerTiltas())		{CANTxdata[4] |= (1<<4);} else{CANTxdata[4] &= ~(1 << 4);}

}

//Gy�jt�csiga
void setCANGyujtocsigaMehet(void)
{
 	CANTxdata[4] |= (1 << 1);
}

void clearCANGyujtocsigaMehet(void)
{
	CANTxdata[4] &= ~(1 << 1);
}

int getCANGyujtocsigaMehet(void)
{
	if((CANTxdata[4] & (1<< 1)) > 0)return 1;
	else return 0;
}



//K�ls� salakol�
void setCANSalakoloMehet(void)
{
 	CANTxdata[4] |= (1 << 0);
}

void clearCANSalakoloMehet(void)
{
	CANTxdata[4] &= ~(1 << 0);
}

int getCANSalakoloMehet(void)
{
	if((CANTxdata[4] & (1<< 0)) > 0)return 1;
	else return 0;
}



//�kl�tra mehet
void setCANEkletraMehet(void)
{
 	CANTxdata[4] |= (1 << 2);
}

void clearCANEkletraMehet(void)
{
	CANTxdata[4] &= ~(1 << 2);
}

int getCANEkletraMehet(void)
{
	if((CANTxdata[4] & (1<< 2)) > 0)return 1;
	else return 0;
}

//////////////
//�ppen t�lt
void setCANTolt(void)
{
 	CANTxdata[4] |= (1 << 6);
}

void clearCANTolt(void)
{
	CANTxdata[4] &= ~(1 << 6);
}

int getCANETolt(void)
{
	if((CANTxdata[4] & (1<< 6)) > 0)return 1;
	else return 0;
}

//�ppen salakol
void setCANSalakol(void)
{
 	CANTxdata[4] |= (1 << 7);
}

void clearCANSalakol(void)
{
	CANTxdata[4] &= ~(1 << 7);
}

int getCANESalakol(void)
{
	if((CANTxdata[4] & (1<< 7)) > 0)return 1;
	else return 0;
}





//Hiba�zenetek

void setCANSzentartalyUres(void)
{
 	CANTxdata[6] |= (1 << 1);
}

void clearCANSzentartalyUres(void)
{
	CANTxdata[6] &= ~(1 << 1);
}

int getCANSzentartalyUres(void)
{
	if((CANTxdata[6] & (1<< 1)) > 0)return 1;
	else return 0;
}
////////////
void setCANVesztermosztat(void)
{
 	CANTxdata[6] |= (1 << 2);
}

void clearCANVesztermosztat(void)
{
	CANTxdata[6] &= ~(1 << 2);
}

int getCANVesztermosztat(void)
{
	if((CANTxdata[6] & (1<< 2)) > 0)return 1;
	else return 0;
}
////////////
void setCANKazanKihult(void)
{
 	CANTxdata[6] |= (1 << 3);
}

void clearCANKazanKihult(void)
{
	CANTxdata[6] &= ~(1 << 3);
}

int getCANKazanKihult(void)
{
	if((CANTxdata[6] & (1<< 3)) > 0)return 1;
	else return 0;
}
////////////
void setCANToltoHiba(void)
{
 	CANTxdata[6] |= (1 << 5);
}

void clearCANToltoHiba(void)
{
	CANTxdata[6] &= ~(1 << 5);
}

int getCANToltoHiba(void)
{
	if((CANTxdata[6] & (1<< 5)) > 0)return 1;
	else return 0;
}

void setCANSalakoloHiba(void)
{
 	CANTxdata[6] |= (1 << 6);
}

void clearCANSalakoloHiba(void)
{
	CANTxdata[6] &= ~(1 << 6);
}

int getCANSalakoloHiba(void)
{
	if((CANTxdata[6] & (1<< 6)) > 0)return 1;
	else return 0;
}



int getCANHidraulikaNyomas(void)
{
 	return ((CANRxdata80[0]<<8) + CANRxdata80[1]);
}

//�kl�tra �zenetek
int getCANSalakoloOK(void)
{
	int i = getKazanNum();
	
	if((CANRxdata50[5] & (1<< i)) > 0)return 1;
	else return 0;
}

//�kl�tra hiba�zenetek

int getCANEkletraErrorCsiga1Elakadt(void)
{
	if((CANRxdata50[0] & (1<< 1)) > 0)return 1;
	else return 0;
}

int getCANEkletraErrorElakadt(void)
{
	if((CANRxdata50[0] & (1<< 7)) > 0)return 1;
	else return 0;
}

int getCANEkletraErrorIdotullepes(void)
{
	if((CANRxdata50[1] & (1<< 0)) > 0)return 1;
	else return 0;
}

int getCANKulsoSalakoloError(void)
{
	if((CANRxdata50[1] & (1<< 6)) > 0)return 1;
	else return 0;
}
	 