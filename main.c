#include <stdio.h>                          
#include "AT91SAM7X256.H"
#include <rtl.h>
#include "main.h"
#include "Board.h"
#include "data.h"
#include "kazan.h"
#include "Tolto.h"
#include "max6675.h"
#include "error.h"
#include "shift.h"
#include "expander.h"
#include "CAN_Cfg.h"                  
#include "RTX_CAN.h"
#include "CAN_data.h"



volatile AT91PS_ADC pADC = AT91C_BASE_ADC;
AT91S_AIC * pAIC = AT91C_BASE_AIC;

U32 Rx_val1,Rx_val2;

volatile int i,ii,out1_state,out_state,kazanNum;
volatile int dispTimer, dispOn;
volatile int tenth,sec,minute,hour;
volatile int turn_off_timer,turn_off,start_lekapcs;
volatile unsigned int AD_val,AD_val_tmp,AD1_val,AD1_val_tmp,AD2_val,AD2_val_tmp,AD3_val,AD3_val_tmp;
volatile int AD_measure_count;
volatile int is_pwm0_on;
volatile int pwm0_value = 150;
volatile int pwm0_start;
volatile int Bolygato_duty;

volatile int comm_timeout,comm_ptr;
volatile int powerComm_timeout,powerComm_ptr;
volatile int bekapcs_timer = 0;
volatile int WDstate;

OS_TID t_main_task,t_comm_task,t_powerCom_task,t_time_task,t_task_send_CAN,t_task_rece_CAN;

extern void init_serial(void);  
extern void sendchar0(int);
extern void sendchar1(int);
extern int getkey0(void);
extern int getkey1(void);
extern void kazan(void);

//Read ADC
void ReadAdc(void)
{ 
		*AT91C_ADC_CR   = AT91C_ADC_START; 
		AD_val = ((*AT91C_ADC_CDR7 & 0x3FF)/4);
		AD1_val = ((*AT91C_ADC_CDR4 & 0x3FF)/4);
		AD2_val = ((*AT91C_ADC_CDR5 & 0x3FF)/4);
		AD3_val = ((*AT91C_ADC_CDR6 & 0x3FF)/4);
    	setBolygatoArammero(AD_val);
		setToltoajtoArammero(AD1_val);
		setPernyeajtoArammero(AD2_val);
		setSalakajtoArammero(AD3_val);
}
//----------------------------------------------------------------------------------
//PWM update
void PWM_update(void)
{
    if(is_pwm0_on)
        {
            if(AD_val < getBolygatoAramkorlat())
            {
                 if(pwm0_value > 0){pwm0_value--;}
            }
            if(AD_val >= getBolygatoAramkorlat())
            {
                 if(pwm0_value < 255){pwm0_value++;}
            }
            
            
        }

        else{pwm0_value = 255;}

        AT91C_BASE_PWMC_CH0->PWMC_CUPDR = pwm0_value;
}
//PWM0_ON
void pwm0_on(void)
{
    is_pwm0_on = 1;
    if(pwm0_start == 0){pwm0_value = 255;pwm0_start = 1;}
}
//PWM0_OFF
void pwm0_off(void)
{
    is_pwm0_on = 0;
    pwm0_start = 0;
}
//---------------------------------------------------------------------------------
//Main task
__task void main_task(void)
{
	int x;
	unsigned int value,temp_count;

	if(AT91C_BASE_RSTC->RSTC_RSR & AT91C_RSTC_RSTTYP_USER){setWatchdogError();}			//Ha lefagy�s miatt indult �jra
	
	for(x=0;x<6;x++)				//Kijelz� bekapcsol�s k�sleltet�se
	{
		//WatchdogPulse;
		AT91C_BASE_WDTC->WDTC_WDCR = 0XA5000001;
		os_dly_wait(5);
	}
	//set_outputs(15);

	loadData();

 	while(true)
	{
		AT91C_BASE_WDTC->WDTC_WDCR = 0XA5000001;

		if(++temp_count > 10)
		{
			//value = getCANHidraulikaNyomas();
			value = readMax6675(0);
			//value = getNyomasmero();
					
			setHomero(0,value);
			value = readMax6675(1);
			setHomero(1,value);
			value = readMax6675(2);	
			
			setHomero(2,value); 
			temp_count = 0;

			/*if(getGyujtHiba()){setCANGyujtottHiba();OUT2_ON;}
			else{clearCANGyujtottHiba();OUT2_OFF;}*/

			if(AKKU_OK){clearAkkuHianyzik();}else{setAkkuHianyzik();}	
		}		
	
		//WatchdogPulse;
		kazan();
		shift();
		Tolto();
		Salakolo();

        refreshData();
		refreshCANdata();
        PWM_update();
        

        //if(!ARAMSZUNET){turn_off_timer = 0;}
		if(BOVITO_LEKAPCS && BOVITO_ARAMSZUNET){turn_off_timer = 0;}
		if(!BOVITO_LEKAPCS && BOVITO_ARAMSZUNET)
		{
			if(!start_lekapcs)
			{
				start_lekapcs = 1;
				turn_off_timer = 15;
			}
		}


        if(turn_off_timer > 30){turn_off = 1;}

		if(!BOVITO_ARAMSZUNET){setBovitoAramszunetError();}
		else{clearBovitoAramszunetError();}

		os_dly_wait(3); 
	}
}
//-----------------------------------------------------------------------------
//Communication task
__task void comm_task(void)						
{
	int x;
	int sendBlockID = 0X01;
 	while(true)
	{
		if(comm_timeout > 1)
		{ 
			if((comm_ptr == 160)&&(rx_buff[157] == 0XFA)&&(rx_buff[158] == 0XFA)&&(rx_buff[159] == 0XFA))
			{  
				set_rx_Data(rx_buff);
			}

            set_tx_Data(tx_buff);
			
			for(x=0;x<100;x++)
			{
			 	sendchar1(tx_buff[x]);
			}

			comm_timeout = 0;
			comm_ptr = 0;

		}
		comm_timeout++;
		os_dly_wait(5); 
	}

}
//------------------------------------------------------------------------------

//Power supplyommunication task
__task void powerComm_task(void)						
{
	int x;
 	while(true)
	{
		if(powerComm_timeout > 1)
		{ 
			if((powerComm_ptr == 10)&&(rx_buff[0] == 0XA5)&&(rx_buff[1] == 0X5A))
			{  
				set_rx_Data(power_rx_buff);
			}

			power_tx_buff[0] = 0X5A;
			power_tx_buff[1] = 0XA5;
			
      set_power_tx_Data(power_tx_buff);
			
			for(x=0;x<10;x++)
			{
			 	sendchar0(power_tx_buff[x]);
			}

			
			powerComm_timeout = 0;
			powerComm_ptr = 0;

		}
		powerComm_timeout++;
		os_dly_wait(10); 
	}

}
//------------------------------------------------------------------------------

//Time task
__task void time_task(void)
{
	volatile int state = 0;
	while(true)
	{
    	kazanTenthTimer();
		toltoTenthTimer();
		salakTenthTimer();
    	ReadAdc();

		if(!WDstate)
		{
		   WatchdogPulse_ON;
		   WDstate=1;
		}
		else
		{
		 	
		   WatchdogPulse_OFF;
		   WDstate=0;
		}

		if(turn_off)
		{
			KIKAPCS_ON;			   
		}else{KIKAPCS_OFF;}
		
        
 	 	if(++tenth ==10)
		{			
		 	tenth = 0;
      		kazanSecTimer();
			salakSecTimer();							
			toltoSecTimer();
      		turn_off_timer++;
			bekapcs_timer++;
      		error();
			saveData();

			if(!dispOn){dispTimer++;}													//Kijelz� k�sleltetett bekapcsol�sa 
			if(dispTimer > 3){dispOn = 1;set_outputs(15);}

			
			if(++sec == 60)
			{
			 	sec = 0;
				salakMinTimer();
				toltoMinTimer();
				clearWatchdogError();
				if(++minute == 60)
				{
				 	minute = 0;
					if(++hour == 24)
					{
					 	hour = 0;
					}
				}
			}
		}

	    os_dly_wait(10);
	 }
}
//------------------------------------------------------------------------------
//CAN Send
task_send_CAN (void)
{
  CAN_msg msg_send       = { 10, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},   
                              8, 1, STANDARD_FORMAT, DATA_FRAME }; 

  CAN_init (1, 100000);                 
  CAN_rx_object (1, 2,  90, DATA_TYPE | STANDARD_TYPE);
  CAN_rx_object (1, 3,  50, DATA_TYPE | STANDARD_TYPE); 
                                                           
  CAN_start (1);                      
  os_dly_wait (10);                   
  

  while(1)
  {
	kazanNum = (getKazanNum() + 10);

  	setCANtx_data(0X00,msg_send.data);
	msg_send.id = kazanNum;
	CAN_send (1, &msg_send, 0x00F0);

	os_dly_wait (5);

	setCANtx_data(0X01,msg_send.data);
	msg_send.id = kazanNum;
	CAN_send (1, &msg_send, 0x00F0);

	os_dly_wait (45);
  }
  	
}
//------------------------------------------------------------------------------
//CAN Receive
task_rece_CAN (void)
{
 	CAN_msg msg_rece;
	os_dly_wait (10);
	
	while(1)
	{
	 	if (CAN_receive (1, &msg_rece, 0x000F) == CAN_OK) 
		{	
			if(msg_rece.id == 90)				//Nyom�sm�r�
			{setHidraulikaNyomasmero(msg_rece.data);}

			if(msg_rece.id == 50)				//�kl�tra
			{setCANrx_data(50,msg_rece.data);}			
		}
		os_dly_wait(1);
	}
}
//------------------------------------------------------------------------------

//Init tasks
__task void init(void)
 {
	   t_main_task = os_tsk_create (main_task, 2);
	   t_comm_task = os_tsk_create (comm_task, 2);
	//	t_comm_task = os_tsk_create (powerComm_task, 2);
	   t_time_task = os_tsk_create (time_task, 4);
	   t_task_send_CAN = os_tsk_create (task_send_CAN, 4);
	   t_task_rece_CAN = os_tsk_create (task_rece_CAN, 1);

	   os_tsk_delete_self ();

 }
//-------------------------------------------------------------------------------
 //USART0 IRQ
__irq void usart0_irq(void)
{
	int rx_char;
	powerComm_timeout = 0;
	rx_char = getkey0();
	power_rx_buff[powerComm_ptr] = rx_char;
	if(powerComm_ptr < 10){powerComm_ptr++;}
	pAIC->AIC_EOICR = 0;
}
//--------------------------------------------------------------------------------
//USART1 IRQ
__irq void usart1_irq(void)
{
	int rx_char;
	comm_timeout = 0;
	rx_char = getkey1();
	rx_buff[comm_ptr] = rx_char;
	if(comm_ptr < 160){comm_ptr++;}
	pAIC->AIC_EOICR = 0;
}
//--------------------------------------------------------------------------------

//main
int main()
{
	unsigned char rtc_data[100];
	int x;
    setBolygBe(5);                          //Ha a kijelz� nem m�k�dik a bolygat�s att�l m�g mehet
	//setAutomata();							//Ha kijelz� nem m�k�dik, automat�n megy tov�bb

 	AT91C_BASE_PIOA->PIO_PER = 0XFFE7E108;
	AT91C_BASE_PIOA->PIO_OER = 0X5FA06108;
	AT91C_BASE_PIOA->PIO_PPUER = 0X20478000;
	AT91C_BASE_PIOA->PIO_PDR = 0X00180C63;
	AT91C_BASE_PIOA->PIO_ASR = 0X00180063;

	AT91C_BASE_PIOB->PIO_PER = 0XFBF7F3F3;
  	AT91C_BASE_PIOB->PIO_PDR = 0X00080000;
	AT91C_BASE_PIOB->PIO_OER = 0XF1F41093;
  //AT91C_BASE_PIOB->PIO_ODR = 0X00020000;
	AT91C_BASE_PIOB->PIO_PPUER = 0X0A03E360;
  	AT91C_BASE_PIOB->PIO_ASR = 0X00080000;	 

	*AT91C_PMC_PCER  = ((1 << AT91C_ID_PIOA)|(1 << AT91C_ID_PIOB)|(1 << AT91C_ID_US0)|(1 << AT91C_ID_US1)|(1 << AT91C_ID_PWMC)|(1 << AT91C_ID_TWI));

    //AT91C_BASE_AIC->AIC_SMR[AT91C_ID_PWMC] = 0;
    //AT91C_BASE_AIC->AIC_SVR[AT91C_ID_PWMC] = (unsigned int) ISR_Pwmc;

	pAIC->AIC_SMR[AT91C_ID_US0] = AT91C_AIC_SRCTYPE_INT_POSITIVE_EDGE | 7;    
	pAIC->AIC_SVR[AT91C_ID_US0] = (unsigned long) usart0_irq;
	AT91C_BASE_AIC->AIC_IECR = (1 << AT91C_ID_US0);
		
	pAIC->AIC_SMR[AT91C_ID_US1] = AT91C_AIC_SRCTYPE_INT_POSITIVE_EDGE | 7;    
	pAIC->AIC_SVR[AT91C_ID_US1] = (unsigned long) usart1_irq;
	AT91C_BASE_AIC->AIC_IECR = (1 << AT91C_ID_US1);

 
    //AT91C_BASE_PWMC->PWMC_IER = 1 << 0;
    
     *AT91C_ADC_MR   = (AT91C_ADC_TRGEN_DIS |            
                      AT91C_ADC_SLEEP_NORMAL_MODE |
                      (8 <<  8) |		//prescal                      
                      (20 << 16) |  	//startup                  
                      (20   << 24)  	//shtim                        
                      );  

	pADC->ADC_CHER = AT91C_ADC_CH4 | AT91C_ADC_CH5 | AT91C_ADC_CH6 | AT91C_ADC_CH7; 

	AT91C_BASE_PWMC->PWMC_ENA = 1;
	AT91C_BASE_PWMC->PWMC_MR = 0X00000A08;
  	AT91C_BASE_PWMC_CH0->PWMC_CMR = 0x000A;
  	AT91C_BASE_PWMC_CH0->PWMC_CPRDR = 255;
  	AT91C_BASE_PWMC_CH0->PWMC_CDTYR = 254;
    
	CS0_HI;CS1_HI;CS2_HI;

	RTC_DS_HI;
	RTC_CLK_HI;RTC_RST_LO;

	init_serial();
	
	P2_ON;
	toltoInit();
	salakInit();


	os_sys_init (init);

	while(true)
	{
		;
	}

}
