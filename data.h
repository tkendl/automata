#ifndef __DATA_H
#define __DATA_H
  


void set_tx_Data(unsigned char[]);
void set_rx_Data(unsigned char[]);

void loadData(void);
void saveData(void);

//01
int getHofok1(void);
int getVizhoHyst(void);
int getRostelyseb(void);
int getUzemiBolyg(void);
int getAlloBolyg(void);
int getBolygBe(void);
void setBolygBe(int);
int getBolygatoAramkorlat(void);
int getTulfutHyst(void);
int getKihulHyst(void);
int getKihulIdo(void);
int getTimerBe(void);
int getTimerSzunet(void);
void setNyomasmero(unsigned char[]);
int getNyomasmero(void);

//02
int getImitaltToltes(void);
int getIdotullepes(void);
int getProbalkozasSzam(void);
int getToltoajtoAramkorlat(void);
int getPernyecsigaAramkorlat(void);
int getPockoloAramkorlat(void);
int getPernyezoForgido(void);
int getPockoloForgido(void);
int getSalakoloForgido(void);
int getTolto1Forgido(void);
int getTolto2Forgido(void);
int getFelhordo1Forgido(void);
int getFelhordo2Forgido(void);
int getTeritoForgido(void);
int getSalakBe(void);
int getSalakSzunet(void);
int getPernyeFelfutas(void);
int getPockoloFelfutas(void);
int getSalakoloFelfutas(void);
int getTolto1Felfutas(void);
int getTolto2Felfutas(void);
int getFelhordo1Felfutas(void);
int getFelhordo2Felfutas(void);
int getTeritoFelfutas(void);
int getFelhordoUtanfutas(void);
int getKazanNum(void);
int getToltesSebesseg(void);

int getToltoajtoArammero(void);
int getSalakajtoArammero(void);
int getPernyeajtoArammero(void);


void setSalakActIdo(int);
void setSalakszunetActIdo(int);
void setImitActIdo(int);

//bites v�ltoz�k	
int getFauzem(void);
int getMelegentart(void);
int getRoomTherPresent(void);
int getFusthomeroPresent(void);
int getLowlevelEnable(void);
int getBurninEnable(void);
int getBurninAlarm(void);
int getPumpPresent(void);
int getScheduleMode(void);
int getThreephaseVentil(void);
int getBiokazan(void);
int	getGozos(void);						
int getBolygato230V(void);
int	getToltoInstalled(void);			
int	getToltocsiga2Installed(void);		
int	getSalakoloInstalled(void);			
int	getFelhordoInstalled(void);			
int	getFelhordocsiga2Installed(void);	
int	getToltorendszerTiltas(void);
int getSalakolorendszerTiltas(void);
int getKazanTiltas(void);
int getSzelloztetes(void);




//Gombok
int getKezi(void);
int getRostelyGomb(void);
int getVentiGomb(void);
int getBolygGomb(void);
int getBelimoGomb(void);
int getSzivattyuGomb(void);
int getClearGomb(void);
int getCsengetes(void);
int getSheduleStart(void);
int getNyitasGomb(void);
int getZarasGomb(void);
int getTeritoEloreGomb(void);
int getTeritoHatraGomb(void);
int getPernyeEloreGomb(void);
int getPernyeHatraGomb(void);
int getPockoloEloreGomb(void);
int getPockoloHatraGomb(void);
int getTolto1EloreGomb(void);
int getTolto1HatraGomb(void);
int getTolto2EloreGomb(void);
int getTolto2HatraGomb(void);
int getSalakoloEloreGomb(void);
int getSalakoloHatraGomb(void);
int getFelhordo1EloreGomb(void);
int getFelhordo1HatraGomb(void);
int getFelhordo2EloreGomb(void);
int getFelhordo2HatraGomb(void);
int getRazomotorGomb(void);
int getEkletraMehetGomb(void);

int getFelautomata(void);
int getAutomata(void);

//Bemenetek 
void setInputPin(int,int);
void clearInputPin(int,int);

//Kimenetek
void setOutputPin(int,int);
void clearOutputPin(int,int);

void refreshData(void);
void setHomero(int,int);
int getHomero1(void);
int getHomero2(void);
int getHomero3(void);
void setArammero(int);
int getArammero(void);



//Hiba�zenetek
void setKihultError(void);
void clearKihultError(void);
int getKihultError(void);

void setWatchdogError(void);
void clearWatchdogError(void);
int getWatchdogError(void);

void setOverheatError(void);
void clearOverheatError(void);
int getOverheatError(void);

void setTuzoltasError(void);
void clearTuzoltasError(void);
int getTuzoltasError(void);

void setAkkuNemOK(void);
void clearAkkuNemOK(void);
int getAkkuNemOK(void);

void setVentilatorMVError(void);
void clearVentilatorMVError(void);
int getVentilatorMVError(void);

void setBolygatoMVError(void);
void clearBolygatoMVError(void);
int getBolygatoMVError(void);

void setTolt1ProbalError(void);
void clearTolt1ProbalError(void);
int getTolt1ProbalError(void);

void setTolt2ProbalError(void);
void clearTolt2ProbalError(void);
int getTolt2ProbalError(void);

void setTeritoProbalError(void);
void clearTeritoProbalError(void);
int getTeritoProbalError(void);

void setFelh1ProbalError(void);
void clearFelh1ProbalError(void);
int getFelh1ProbalError(void);

void setFelh2ProbalError(void);
void clearFelh2ProbalError(void);
int getFelh2ProbalError(void);

void setSalakProbalError(void);
void clearSalakProbalError(void);
int getSalakProbalError(void);

void setTolt1ElakadtError(void);
void clearTolt1ElakadtError(void);
int getTolt1ElakadtError(void);

void setTolt2ElakadtError(void);
void clearTolt2ElakadtError(void);
int getTolt2ElakadtError(void);

void setTeritoElakadtError(void);
void clearTeritoElakadtError(void);
int getTeritoElakadtError(void);

void setFelh1ElakadtError(void);
void clearFelh1ElakadtError(void);
int getFelh1ElakadtError(void);

void setFelh2ElakadtError(void);
void clearFelh2ElakadtError(void);
int getFelh2ElakadtError(void);

void setSalakElakadtError(void);
void clearSalakElakadtError(void);
int getSalakElakadtError(void);

void setPockoloElakadtError(void);
void clearPockoloElakadtError(void);
int getPockoloElakadtError(void);

void setPernyeElakadtError(void);
void clearPernyeElakadtError(void);
int getPernyeElakadtError(void);


void setTolt1MVError(void);
void clearTolt1MVError(void);
int getTolt1MVError(void);

void setTolt2MVError(void);
void clearTolt2MVError(void);
int getTolt2MVError(void);

void setTeritoMVError(void);
void clearTeritoMVError(void);
int getTeritoMVError(void);

void setFelh1MVError(void);
void clearFelh1MVError(void);
int getFelh1MVError(void);

void setFelh2MVError(void);
void clearFelh2MVError(void);
int getFelh2MVError(void);

void setSalakMVError(void);
void clearSalakMVError(void);
int getSalakMVError(void);



void setTolt1MVnemkapcsoltError(void);
void clearTolt1MVnemkapcsoltError(void);
int getTolt1MVnemkapcsoltError(void);

void setTolt2MVnemkapcsoltError(void);
void clearTolt2MVnemkapcsoltError(void);
int getTolt2MVnemkapcsoltError(void);

void setTeritoMVnemkapcsoltError(void);
void clearTeritoMVnemkapcsoltError(void);
int getTeritoMVnemkapcsoltError(void);

void setFelh1MVnemkapcsoltError(void);
void clearFelh1MVnemkapcsoltError(void);
int getFelh1MVnemkapcsoltError(void);

void setFelh2MVnemkapcsoltError(void);
void clearFelh2MVnemkapcsoltError(void);
int getFelh2MVnemkapcsoltError(void);

void setSalakMVnemkapcsoltError(void);
void clearSalakMVnemkapcsoltError(void);
int getSalakMVnemkapcsoltError(void);
//
void setAjtoProbalError(void);
void clearAjtoProbalError(void);
int getAjtoProbalError(void);

void setAjtoNemnyitottError(void);
void clearAjtoNemnyitottError(void);
int getAjtoNemnyitottError(void);

void setAjtoNemzartError(void);
void clearAjtoNemzartError(void);
int getAjtoNemzartError(void);

void setAjtoNyitvakapcsoloRossz(void);
void clearAjtoNyitvakapcsoloRossz(void);
int getAjtoNyitvakapcsoloRossz(void);

void setAjtoZarvakapcsoloRossz(void);
void clearAjtoZarvakapcsoloRossz(void);
int getAjtoZarvakapcsoloRossz(void);

void setSzintjelzoHiba(void);
void clearSzintjelzoHiba(void);
int getSzintjelzoHiba(void);

void setIdotullepesHiba(void);
void clearIdotullepesHiba(void);
int getIdotullepesHiba(void);

void setPockoloProbalError(void);
void clearPockoloProbalError(void);
int getPockoloProbalError(void);

void setPernyeProbalError(void);
void clearPernyeProbalError(void);
int getPernyeProbalError(void);

void setVedoberendezesekError(void);
void clearVedoberendezesekError(void);
int getVedoberendezesekError(void);

void setBovitoAramszunetError(void);
void clearBovitoAramszunetError(void);
int getBovitoAramszunetError(void);


#endif
