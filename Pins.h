

 #ifndef __PINS_H
 #define __PINS_H

void _teritoElore(void);
void _teritoHatra(void);
void _teritoAllj(void);
int _teritoMV(void);
int _teritoFF(void);

void _tolt1Elore(void);
void _tolt1Hatra(void);
void _tolt1Allj(void);
int _tolt1MV(void);
int _tolt1FF(void);

void _tolt2Elore(void);
void _tolt2Hatra(void);
void _tolt2Allj(void);
int _tolt2MV(void);
int _tolt2FF(void);

void _pockoloElore(void);
void _pockoloHatra(void);
void _pockoloAllj(void);
int _pockoloMV(void);
int _pockoloFF(void);

void _pernyeElore(void);
void _pernyeHatra(void);
void _pernyeAllj(void);
int _pernyeMV(void);
int _pernyeFF(void);

void _salakElore(void);
void _salakHatra(void);
void _salakAllj(void);
int _salakMV(void);
int _salakFF(void);

void _felhordo1Elore(void);
void _felhordo1Hatra(void);
void _felhordo1Allj(void);
int _felhordo1MV(void);
int _felhordo1FF(void);

void _felhordo2Elore(void);
void _felhordo2Hatra(void);
void _felhordo2Allj(void);
int _felhordo2MV(void);
int _felhordo2FF(void);


 #endif