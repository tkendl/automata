


#ifndef __AJTO_H
#define __AJTO_H

#define AJTO_NOT_OK					0
#define	AJTO_OK						1
#define	AJTO_PROBALKOZIK			2
#define	AJTO_ELAKADT				3

#define AJTOKAPCSOLO_TIMEOUT		100	

int Ajto(void);
void ajtoTimer(void);

void ajtoNyitas(void);
void ajtoCsukas(void);
void ajtoAllj(void);	
int getAjtoNyitva(void);
int getAjtoCsukva(void);
int getAjtoHiba(void);




#endif