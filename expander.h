

#ifndef __EXPANDER_H
#define __EXPANDER_H

//#define	PROBA

void set_expander_outputs(int);
void clear_expander_outputs(int);
int get_expander_inputs(int);

#define AJTO_NYITAS	set_expander_outputs(3);clear_expander_outputs(2); //P2_ON;
#define AJTO_ZARAS 	set_expander_outputs(2);clear_expander_outputs(3); //P2_ON;
#define AJTO_STOP 	clear_expander_outputs(2);clear_expander_outputs(3);//P2_OFF;

#define TOLTO1_ELORE_ON		set_expander_outputs(10);clear_expander_outputs(20);
#define TOLTO1_ELORE_OFF	clear_expander_outputs(10);

#define TOLTO1_HATRA_ON		set_expander_outputs(20);clear_expander_outputs(10);
#define TOLTO1_HATRA_OFF	clear_expander_outputs(20);

#define TOLTO2_ELORE_ON		set_expander_outputs(12);clear_expander_outputs(18);
#define TOLTO2_ELORE_OFF	clear_expander_outputs(12);

#define TOLTO2_HATRA_ON		set_expander_outputs(18);clear_expander_outputs(12);
#define TOLTO2_HATRA_OFF	clear_expander_outputs(18);

#define TERITO_ELORE_ON		set_expander_outputs(11);clear_expander_outputs(19);
#define TERITO_ELORE_OFF	clear_expander_outputs(11);

#define TERITO_HATRA_ON		set_expander_outputs(19);clear_expander_outputs(11);
#define TERITO_HATRA_OFF	clear_expander_outputs(19);

//#define RAZOMOTOR_ON		set_expander_outputs(10);
//#define RAZOMOTOR_OFF		clear_expander_outputs(10);

#define SALAKOLO_ELORE_ON	set_expander_outputs(8);clear_expander_outputs(22);
#define SALAKOLO_ELORE_OFF	clear_expander_outputs(8);

#define SALAKOLO_HATRA_ON	set_expander_outputs(22);clear_expander_outputs(8);
#define SALAKOLO_HATRA_OFF	clear_expander_outputs(22);

#define FELHORDO1_ELORE_ON	set_expander_outputs(9);clear_expander_outputs(21);
#define FELHORDO1_ELORE_OFF	clear_expander_outputs(9);

#define FELHORDO1_HATRA_ON	set_expander_outputs(21);clear_expander_outputs(9);
#define FELHORDO1_HATRA_OFF	clear_expander_outputs(21);

#define FELHORDO2_ELORE_ON	set_expander_outputs(25);clear_expander_outputs(25);
#define FELHORDO2_ELORE_OFF	clear_expander_outputs(25);

#define FELHORDO2_HATRA_ON	set_expander_outputs(25);clear_expander_outputs(25);
#define FELHORDO2_HATRA_OFF	clear_expander_outputs(25);




#define AJTO_CSUKVA							get_expander_input(1)
#define	AJTO_NYITVA							get_expander_input(2)
#define	PERNYE_FORGASFIGYELO		get_expander_input(5)
#define POCKOLO_FORGASFIGYELO		get_expander_input(6)
#define AKKU_OK									get_expander_input(7)
#define	FELHORDO2_FORGASFIGYELO	get_expander_input(8)
#define	FELHORDO1_FORGASFIGYELO	get_expander_input(9)
#define	SALAKOLO_FORGASFIGYELO	get_expander_input(10)
#define	TERITO_FORGASFIGYELO		get_expander_input(12)
#define	TOLTO2_FORGASFIGYELO		get_expander_input(13)
#define	TOLTO1_FORGASFIGYELO		get_expander_input(14)


#ifdef 	PROBA

#define	VEDOBERENDEZESEK_KORE	1
#define SALAKOLO_MOTORVEDO		1
#define FELHORDO1_MOTORVEDO		1
#define TOLTO1_MOTORVEDO		1
#define TERITO_MOTORVEDO		1
#define FELHORDO2_MOTORVEDO		1
#define TOLTO2_MOTORVEDO		1
#define	BOVITO_ARAMSZUNET		1
#define	BOVITO_LEKAPCS			1

#endif

#ifndef PROBA

#define	VEDOBERENDEZESEK_KORE	get_expander_input(0)
#define SALAKOLO_MOTORVEDO		get_expander_input(15)
#define FELHORDO1_MOTORVEDO		get_expander_input(16)
#define TOLTO1_MOTORVEDO		get_expander_input(17)
#define TERITO_MOTORVEDO		get_expander_input(18)
#define FELHORDO2_MOTORVEDO		get_expander_input(19)
#define TOLTO2_MOTORVEDO		1//get_expander_input(20)
#define	BOVITO_LEKAPCS			get_expander_input(21)
#define	BOVITO_ARAMSZUNET		get_expander_input(22)

#endif




#endif