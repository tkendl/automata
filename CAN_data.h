#ifndef __CAN_DATA_H
#define __CAN_DATA_H

void setCANGyujtocsigaMehet(void);
void clearCANGyujtocsigaMehet(void);
int getCANGyujtocsigaMehet(void);

void setCANEkletraMehet(void);
void clearCANEkletraMehet(void);
int getCANEkletraMehet(void);

void setCANSalakoloMehet(void);
void clearCANSalakoloMehet(void);
int getCANSalakoloMehet(void);


void setCANGyujtottHiba(void);
void clearCANGyujtottHiba(void);
int getCANGyujtottHiba(void);

int getCANHidraulikaNyomas(void);

int getCANSalakoloOK(void);


#endif