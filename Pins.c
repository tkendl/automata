#include "AT91SAM7X256.H"
#include "Board.h"
#include "expander.h"
#include "data.h"

//Ter�t�csiga
void _teritoElore(){TERITO_ELORE_ON;}
void _teritoHatra(){TERITO_HATRA_ON;}
void _teritoAllj(){TERITO_ELORE_OFF;TERITO_HATRA_OFF;}
int _teritoMV()
{
	return 1;
 	/*if(TERITO_MOTORVEDO){return 1;}
	else{return 0;}*/
}

int _teritoFF()
{
 	if(TERITO_FORGASFIGYELO){return 1;}
	else{return 0;}
}

//T�lt� 1
void _tolt1Elore(){TOLTO1_ELORE_ON;}
void _tolt1Hatra(){TOLTO1_HATRA_ON;}
void _tolt1Allj(){TOLTO1_ELORE_OFF;TOLTO1_HATRA_OFF;}
int _tolt1MV()
{
	return 1;
 	/*if (TOLTO1_MOTORVEDO){return 1;}
	else{return 0;}*/
}

int _tolt1FF()
{
 	if (TOLTO1_FORGASFIGYELO){return 1;}
	else{return 0;}
}
//T�lt�2
void _tolt2Elore(){TOLTO2_ELORE_ON;}
void _tolt2Hatra(){TOLTO2_HATRA_ON;}
void _tolt2Allj(){TOLTO2_ELORE_OFF;TOLTO2_HATRA_OFF;}
int _tolt2MV()
{
	return 1;
 	/*if (TOLTO2_MOTORVEDO){return 1;}
	else{return 0;}*/
}

int _tolt2FF()
{
 	if (TOLTO2_FORGASFIGYELO){return 1;}
	else{return 0;}
}
//P�ck�l�
void _pockoloElore(){P1_ON;}
void _pockoloHatra(){P1_OFF;}
void _pockoloAllj(){P1_OFF;}
int _pockoloMV()
{
 	return 1;
}

int _pockoloFF()
{
 	if (POCKOLO_FORGASFIGYELO){return 1;}
	else{return 0;}
}

//Perny�z�
void _pernyeElore(){P0_ON;}
void _pernyeHatra(){P0_OFF;}
void _pernyeAllj(){P0_OFF;}
int _pernyeMV()
{
 	return 1;
}

int _pernyeFF()
{
 	if (PERNYE_FORGASFIGYELO){return 1;}
	else{return 0;}
}

//Salakcsiga
void _salakElore(){SALAKOLO_ELORE_ON;}
void _salakHatra(){SALAKOLO_HATRA_ON;}
void _salakAllj(){SALAKOLO_ELORE_OFF;SALAKOLO_HATRA_OFF;}
int _salakMV()
{
	return 1;
 	/*if(SALAKOLO_MOTORVEDO){return 1;}
	else{return 0;}*/
}

int _salakFF()
{
 	if(SALAKOLO_FORGASFIGYELO){return 1;}
	else{return 0;}
}

//Felhord� 1
void _felhordo1Elore(){FELHORDO1_ELORE_ON;}
void _felhordo1Hatra(){FELHORDO1_HATRA_ON;}
void _felhordo1Allj(){FELHORDO1_ELORE_OFF;FELHORDO1_HATRA_OFF;}
int _felhordo1MV()
{
	return 1;
 	/*if (FELHORDO1_MOTORVEDO){return 1;}
	else{return 0;}*/
}

int _felhordo1FF()
{
 	if (FELHORDO1_FORGASFIGYELO){return 1;}
	else{return 0;}
}
//Felhord� 2
void _felhordo2Elore(){FELHORDO2_ELORE_ON;}
void _felhordo2Hatra(){FELHORDO2_HATRA_ON;}
void _felhordo2Allj(){FELHORDO2_ELORE_OFF;FELHORDO2_HATRA_OFF;}
int _felhordo2MV()
{
 	if (FELHORDO2_MOTORVEDO){return 1;}
	else{return 0;}
}

int _felhordo2FF()
{
 	if (FELHORDO2_FORGASFIGYELO){return 1;}
	else{return 0;}
}
