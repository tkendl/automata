#include "AT91SAM7X256.H"
#include "Ajto.h"
#include "Tolto.h"
#include "csiga.h"
#include "kazan.h"
#include "Board.h"
#include "data.h"
#include "expander.h"

volatile int ajto_timer;
volatile int ajto_kapcsolo_timer;
volatile int ajto_mehet;
volatile int ajto_start;
volatile int ajto_state;
volatile int ajto_probal;
volatile int ajto_error_state;
volatile int ajto_proba_counter;

volatile int _nyitas,_zaras,probal_nyitni, probal_zarni, nem_nyitott, nem_zart, nyitva_kapcsolo_rossz, zarva_kapcsolo_rossz;
volatile int _ajto_nyitas, _ajto_csukas;
volatile int nyitas_start,zaras_start,kezi_tularam;
volatile int keziTimer;

void ajtoTimer(void)
{
 	ajto_timer++;
	ajto_kapcsolo_timer++;
	keziTimer++;
}
												
void ajtoNyitas(void)
{
 	//ajto_mehet = 1;
	_ajto_csukas = 0;
	_ajto_nyitas = 1;
	//ajto_start = 0;
}

void ajtoCsukas(void)
{
 	//ajto_mehet = 0;
	_ajto_nyitas = 0;  
	_ajto_csukas = 1; 
	//ajto_start = 0;
}

void ajtoAllj(void)
{
 	_ajto_nyitas = 0;	
	_ajto_csukas = 0;
	ajto_start = 0;
}

int getAjtoNyitva(void)
{
 	if(AJTO_NYITVA){return 1;}
	else{return 0;}
}

int getAjtoCsukva(void)
{
 	if(AJTO_CSUKVA){return 1;}
	else{return 0;}
}


int Ajto(void)
{
	if(getClearGomb())														//Hibat�rl�s
	{
		ajto_proba_counter = 0;
		ajto_probal = 0;
		probal_nyitni = 0;
		probal_zarni = 0;
		nyitva_kapcsolo_rossz = 0;
		zarva_kapcsolo_rossz = 0;
		clearAjtoProbalError();
		clearAjtoNemnyitottError();
		clearAjtoNemzartError();
	}//hibat�rl�s

 	if(getKezi() > 0)	 													//K�zi
	{	
		if(getNyitasGomb())
		{
			zaras_start = 0;
			if(!nyitas_start){keziTimer = 0;nyitas_start = 1;}
			else{AJTO_STOP;}
			
			if(keziTimer > 10)
			{
				if(AJTO_NYITVA || kezi_tularam){AJTO_STOP;nyitas_start = 0;}
				else{AJTO_NYITAS;}
			}
		}
		
		
		if(getZarasGomb())
		{
			nyitas_start = 0;
			if(!zaras_start){keziTimer = 0;zaras_start = 1;}
			else{AJTO_STOP;}
			
			if(keziTimer > 10)
			{
				if(AJTO_CSUKVA || kezi_tularam){AJTO_STOP;zaras_start = 0;}
				else{AJTO_ZARAS;}
			}
		}

		if(getToltoajtoArammero() > getToltoajtoAramkorlat())
		{
		 	kezi_tularam = 1;	
		}

		if((!getNyitasGomb())&&(!getZarasGomb())){AJTO_STOP;kezi_tularam = 0;nyitas_start = 0;zaras_start = 0;}
	}  //K�zi



	if(getKezi() == 0)																	//Automata/f�lautomata
	{	
		if(_ajto_nyitas && !ajto_probal && (ajto_proba_counter <= getProbalkozasSzam()) && !nyitva_kapcsolo_rossz
		&& !getAjtoNemnyitottError())
		{																			   	//norm�l nyit�s
			if(AJTO_NYITVA){AJTO_STOP;ajto_start = 0;_nyitas = 0;}
			else{_nyitas = 1;}	
		}
		else
		{
			if(_ajto_nyitas && (ajto_proba_counter > getProbalkozasSzam()))  			//Ha letelt a nyit�s pr�b�lkoz�sok sz�ma
			{
			 	setAjtoNemnyitottError();												//�llj
				ajtoAllj();																//Z�r�s
				ajtoCsukas();
				ajto_proba_counter = 0;
			}
			_nyitas = 0;
		} 

		if(_ajto_csukas && !ajto_probal && (ajto_proba_counter <= getProbalkozasSzam()) && !zarva_kapcsolo_rossz
		&& !getAjtoNemzartError())
		{																			   	//norm�l z�r�s
			if(AJTO_CSUKVA){AJTO_STOP;ajto_start = 0;_zaras = 0;}
			else{_zaras = 1;}
		}	 
		else
		{
			//
			_zaras = 0;
		}

		if(!_ajto_nyitas	&& !_ajto_csukas)											//ajt� meg�ll�t�sa
		{
		 	_nyitas = 0;
			_zaras = 0;
		}


//nyit�s
		if(_nyitas)
		{	 
			if(ajto_start == 0)
			{	
		 		ajto_start = 1;
				ajto_timer = 0;
				ajto_kapcsolo_timer = 0;
			}
			if(ajto_timer > START_SZUNET)		 						//indul�s el�tt sz�net
			{
				if(!AJTO_NYITVA)										//ha nincs nyitva
				{														//nyit�s
					AJTO_NYITAS;
					if((getToltoajtoArammero() > getToltoajtoAramkorlat()) || (ajto_kapcsolo_timer > AJTOKAPCSOLO_TIMEOUT))	 	//elakad�s figyel�se
					{
						AJTO_STOP;
			 			_nyitas = 0;
						ajto_probal = 1;
						probal_nyitni = 1;
						setAjtoProbalError();
						ajto_start = 0;
						ajto_error_state = AJTO_PROBALKOZIK;
					}
				}
				else									   				//k�l�nben �llj
				{
					AJTO_STOP;
					_nyitas = 0;
					ajto_start = 0;
					ajto_proba_counter = 0;
					ajto_error_state = AJTO_OK;
				}				
				
			}
		 	

			/*if(ajto_kapcsolo_timer > AJTOKAPCSOLO_TIMEOUT)	   			//Nyitva kapcsol� rossz
			{
				AJTO_STOP;
			 	_nyitas = 0;
				nyitva_kapcsolo_rossz = 1;
				ajto_start = 0;
				ajto_error_state = AJTO_NOT_OK;
				ajtoCsukas();

			} */
		}  //_nyitas


//z�r�s
		if(_zaras)
		{	
			if(ajto_start == 0)
			{
		 		ajto_start = 1;
				ajto_timer = 0;
				ajto_kapcsolo_timer = 0;
			}
			if(ajto_timer > START_SZUNET)		 						//indul�s el�tt sz�net
			{
				if(!AJTO_CSUKVA)										//ha nincs z�rva
				{														//z�r�s
					AJTO_ZARAS;
					if((getToltoajtoArammero() > getToltoajtoAramkorlat()) || (ajto_kapcsolo_timer > AJTOKAPCSOLO_TIMEOUT))	 	//elakad�s figyel�se
					{
						AJTO_STOP;
			 			_zaras = 0;
						ajto_probal = 1;
						probal_zarni = 1;
						setAjtoProbalError();
						ajto_start = 0;
						ajto_error_state = AJTO_PROBALKOZIK;
					}
				}													   	//k�l�nben �llj
				else
				{
					AJTO_STOP;
					_zaras = 0;
					ajto_start = 0;
					ajto_proba_counter = 0;
					ajto_error_state = AJTO_OK;
				}				
				
			}
		 	
			
			/*if(ajto_kapcsolo_timer > AJTOKAPCSOLO_TIMEOUT)	   			//Z�rva kapcsol� rossz
			{
				AJTO_STOP; 
			 	_zaras = 0;
				zarva_kapcsolo_rossz = 1;
				ajto_start = 0;
				ajto_error_state = AJTO_NOT_OK;
			}*/	
		}//z�r�s


//pr�b�l nyitni
		if(probal_nyitni == 1)
		{	 
		 	if(ajto_start == 0)
			{
		 		ajto_start = 1;
				ajto_timer = 0;
				ajto_proba_counter++;
			}
			if(ajto_timer > START_SZUNET)		 						//indul�s el�tt sz�net
			{
				if(!AJTO_CSUKVA)										//ha nincs z�rva
				{														//z�r�s
					AJTO_ZARAS;	
				}
				else{AJTO_STOP;probal_nyitni = 0;ajto_probal = 0;ajto_start = 0;clearAjtoProbalError();} //k�l�nben �llj
			}
			
			if(ajto_timer > START_SZUNET + HATRAMENET)		 			//h�tramenet
			{
			 	AJTO_STOP; 
				probal_nyitni = 0;
				ajto_probal = 0;
				ajto_start = 0;	
			}
				
		}//pr�b�l nyitni

//pr�b�l z�rni
		if(probal_zarni == 1)
		{
			if(_ajto_csukas && (ajto_proba_counter > getProbalkozasSzam()))
			{
				setAjtoNemzartError();
				_ajto_csukas = 0; 
				probal_zarni = 0;	
			}

		 	if(ajto_start == 0)
			{
		 		ajto_start = 1;
				ajto_timer = 0;
				ajto_proba_counter++;
			}
			if(ajto_timer > START_SZUNET)		 						//indul�s el�tt sz�net
			{
				if(!AJTO_NYITVA)										//ha nincs z�rva
				{														//z�r�s
					AJTO_NYITAS;
				}
				else{AJTO_STOP;probal_zarni = 0;ajto_probal = 0;ajto_start = 0;clearAjtoProbalError();}		//k�l�nben �llj
			}
			
			if(ajto_timer > START_SZUNET + HATRAMENET)		 			//h�tramenet
			{
			 	AJTO_STOP;
				probal_zarni = 0;
				ajto_probal = 0;
				ajto_start = 0;	
			}
				
		}//pr�b�l z�rni


	} //Automata

	if(!nyitva_kapcsolo_rossz && !zarva_kapcsolo_rossz && !probal_nyitni && !probal_zarni)
	{
	 	ajto_error_state = AJTO_OK;	
	}

	if(nyitva_kapcsolo_rossz || zarva_kapcsolo_rossz || probal_nyitni || probal_zarni)
	{
	 	ajto_error_state = AJTO_NOT_OK;	
	}

	if(nyitva_kapcsolo_rossz){setAjtoNyitvakapcsoloRossz();}
	else{clearAjtoNyitvakapcsoloRossz();}

	if(zarva_kapcsolo_rossz){setAjtoZarvakapcsoloRossz();}
	else{clearAjtoZarvakapcsoloRossz();}


	return ajto_error_state;
}

