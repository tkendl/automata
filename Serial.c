

#include "AT91SAM7X256.H"                        /* AT91SAMT7X256 definitions */
#include "Board.h"

#define AT91B_MAIN_OSC        18432000               // Main Oscillator MAINCK
#define AT91B_MCK             ((18432000*73/14)/2)   // Output PLL Clock

#define BR    38400                         /* Baud Rate */

#define BRD  (AT91B_MCK/16/BR)              /* Baud Rate Divisor */


AT91S_USART * pUSART0 = AT91C_BASE_US0;      /* Global Pointer to USART0 */
AT91S_USART * pUSART1 = AT91C_BASE_US1;      /* Global Pointer to USART1 */


void init_serial (void) {                   /* Initialize Serial Interface */

  *AT91C_PIOA_PDR = AT91C_PA0_RXD0 |        /* Enable RxD0 Pin */
                    AT91C_PA1_TXD0;         /* Enalbe TxD0 Pin */

  *AT91C_PIOA_PDR = AT91C_PA5_RXD1 |        /* Enable RxD1 Pin */
                    AT91C_PA6_TXD1;         /* Enalbe TxD1 Pin */  
//------------------------------------------------------------------------
  pUSART0->US_CR = AT91C_US_RSTRX |          /* Reset Receiver      */
                  AT91C_US_RSTTX |          /* Reset Transmitter   */
                  AT91C_US_RXDIS |          /* Receiver Disable    */
                  AT91C_US_TXDIS;           /* Transmitter Disable */

  pUSART1->US_CR = AT91C_US_RSTRX |          /* Reset Receiver      */
                  AT91C_US_RSTTX |          /* Reset Transmitter   */
                  AT91C_US_RXDIS |          /* Receiver Disable    */
                  AT91C_US_TXDIS;           /* Transmitter Disable */
 //------------------------------------------------------------------------
  pUSART0->US_MR = AT91C_US_USMODE_NORMAL |  /* Normal Mode */
                  AT91C_US_CLKS_CLOCK    |  /* Clock = MCK */
                  AT91C_US_CHRL_8_BITS   |  /* 8-bit Data  */
                  AT91C_US_PAR_NONE      |  /* No Parity   */
                  AT91C_US_NBSTOP_1_BIT;    /* 1 Stop Bit  */

  pUSART1->US_MR = AT91C_US_USMODE_NORMAL |  /* Normal Mode */
                  AT91C_US_CLKS_CLOCK    |  /* Clock = MCK */
                  AT91C_US_CHRL_8_BITS   |  /* 8-bit Data  */
                  AT91C_US_PAR_NONE      |  /* No Parity   */
                  AT91C_US_NBSTOP_1_BIT;    /* 1 Stop Bit  */
 //-------------------------------------------------------------------------
  pUSART0->US_BRGR = BRD;                    /* Baud Rate Divisor */

  pUSART1->US_BRGR = BRD;                    /* Baud Rate Divisor */
 //-------------------------------------------------------------------------
  pUSART0->US_CR = AT91C_US_RXEN  |          /* Receiver Enable     */
                  AT91C_US_TXEN;            /* Transmitter Enable  */

  pUSART1->US_CR = AT91C_US_RXEN  |          /* Receiver Enable     */
                  AT91C_US_TXEN;            /* Transmitter Enable  */
 //-------------------------------------------------------------------------
  pUSART0->US_IER = AT91C_US_RXRDY;

  pUSART1->US_IER = AT91C_US_RXRDY;
}
//--------------------------------------------------------------------------


int sendchar0 (int ch)  {                    /* Write character to Serial Port */

  //if (ch == '\n')  {                            /* Check for CR */
  // while (!(pUSART0->US_CSR & AT91C_US_TXRDY)); /* Wait for Empty Tx Buffer */
  //  pUSART0->US_THR = '\r';                      /* Output CR */
  //}
  while (!(pUSART0->US_CSR & AT91C_US_TXRDY));   /* Wait for Empty Tx Buffer */
  return (pUSART0->US_THR = ch);                 /* Transmit Character */
}
//--------------------------------------------------------------------------
int sendchar1 (int ch)  {                    /* Write character to Serial Port */

  //if (ch == '\n')  {                            /* Check for CR */
   // while (!(pUSART1->US_CSR & AT91C_US_TXRDY)); /* Wait for Empty Tx Buffer */
    //pUSART1->US_THR = '\r';                      /* Output CR */
  //}
  while (!(pUSART1->US_CSR & AT91C_US_TXRDY));   /* Wait for Empty Tx Buffer */
  return (pUSART1->US_THR = ch);                 /* Transmit Character */
}
//----------------------------------------------------------------------------
int usart_kbhit(void)
{
 	if(pUSART1->US_CSR & AT91C_US_TXRDY)
	{
	 	return 1;
	}
	else return 0;
}
//----------------------------------------------------------------------------
int getkey0 (void)  {          	              /* Read character from Serial Port */

  while (!(pUSART0->US_CSR & AT91C_US_RXRDY));   /* Wait for Full Rx Buffer */
  return (pUSART0->US_RHR);                      /* Read Character */
}
//-----------------------------------------------------------------------------
int getkey1 (void)  {          	              /* Read character from Serial Port */

  while (!(pUSART1->US_CSR & AT91C_US_RXRDY));   /* Wait for Full Rx Buffer */
  return (pUSART1->US_RHR);                      /* Read Character */
}
