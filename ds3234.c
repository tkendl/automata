#include "AT91SAM7X256.H"
#include "Board.h"
#include "data.h"

int clkState=0;

void ds_wait()
{
 	int x;
    for(x=0;x<5;x++);
}

void clockPulse()
{
	RTC_CLK_HI;ds_wait();RTC_CLK_LO;ds_wait(); 	
}

int readDS3234(int addr)
{
	int data,x,y;
	
	RTC_DS_LO;ds_wait();RTC_CLK_LO;clkState=0;ds_wait();

	for(x=0;x<8;x++)
	{
		y=7-x;
		
	 	if((addr & (1<<y)) > 0){RTC_DIN_HI;}
		else {RTC_DIN_LO;}
		clockPulse();	
	}

	RTC_DIN_LO;
	data = 0;
			
	for(x=0;x<8;x++)
	{
		y=7-x;
		RTC_CLK_HI;ds_wait();
		if(RTC_DOUT > 0){data |= (1 << y);}
		else {data &= ~(1 << y);}
        RTC_CLK_LO;wait();

	}

	RTC_CLK_LO;
	RTC_DS_HI;
	return data;			 	
}


void writeDS3234(int addr, int data)
{
	int x,y;
	
	RTC_DS_LO;ds_wait();RTC_CLK_LO;clkState=0;ds_wait();

	for(x=0;x<8;x++)
	{
		y=7-x;
		
	 	if((addr & (1<<y)) > 0){RTC_DIN_HI;}
		else {RTC_DIN_LO;}
		clockPulse();	
	}
	RTC_DIN_LO;
	
	for(x=0;x<8;x++)
	{
		y=7-x;
		if((data & (1<<y)) > 0){RTC_DIN_HI;}
		else {RTC_DIN_LO;}
		clockPulse();

	}
	RTC_CLK_LO;
	RTC_DIN_LO;
	RTC_DS_HI;			 	
}