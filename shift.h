
#ifndef __SHIFT_H
#define __SHIFT_H

void shift(void);

void set_outputs(int);
void clear_outputs(int);

int get_expander_input(int);
void set_expander_outputs(int);
void clear_expander_outputs(int);

#endif
